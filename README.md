# AWS Code commit Code deploy and Code pipeline


### Step 1: To create a Code Commit repository
      
1.	Open the Code Commit console at *https://console.aws.amazon.com/codecommit/*.
2.	In the Region selector, choose the AWS Region where you want to create the repository and pipeline. For more information
3.	On the Repositories page, choose Create repository.
4.	On the Create repository page, in Repository name, enter a name for your repository (for example, MyDemoRepo).
5.	Choose Create.

### Step 2: To set up a local repository

1.	With your new repository open in the console, choose Clone URL on the top right of the page, and then choose Clone SSH. The address to clone your Git            repository is copied to your clipboard.
2.	In your terminal or command line, navigate to a local directory where you'd like your local repository to be stored. In this tutorial, we use /tmp.
3.	Run the following command to clone the repository, replacing the SSH address with the one you copied in the previous step. This command creates a directory    called MyDemoRepo. You copy a sample application to this directory.

   git clone ssh://git-codecommit.us-west-2.amazonaws.com/v1/repos/MyDemoRepo

### Step 3: Add Sample Code to Your Code Commit Repository

1.	Download the following file: SampleApp_Linux.zip
2.	Unzip the files from SampleApp_Linux.zip into the local directory you created earlier (for example, /tmp/MyDemoRepo or c:\temp\MyDemoRepo).
    Be sure to place the files directly into your local repository. Do not include a SampleApp_Linux folder. On your local Linux, macOS, or Unix machine, 
3.	Change directories to your local repo
4.	Run the following command to stage all of your files at once:
           git add -A
5.	Run the following command to commit the files with a commit message:
           git commit -m "Add sample application files"
6.	Run the following command to push the files from your local repo to your Code Commit repository:
           git push
7.	The files you downloaded and added to your local repo have now been added to the master branch in your Code Commit MyDemoRepo repository and are ready to be        included in a pipeline.

### Step 4: Create an EC2 Linux Instance and Install the Code Deploy Agent

#### Step 4A :To create an instance role

1. 	Open the IAM console at https://console.aws.amazon.com/iam/).
2.	From the console dashboard, choose Roles.
3.	Choose Create role.
4.	Under Select type of trusted entity, select AWS service. Under Choose a use case, select EC2, and then choose Next: Permissions.
5.	Search for and select the policy named AmazonEC2RoleforAWSCodeDeploy, and then choose Next: Tags.
6.	Choose Next: Review. Enter a name for the role (for example, EC2InstanceRole), and then choose Create role

####   Step 4B: To launch an instance

1.	Open the Amazon EC2 console at https://console.aws.amazon.com/ec2/.
2.	From the console dashboard, choose Launch instance, and select Launch instance from the options that pop up.
3.	On Step 1: Choose an Amazon Machine Image (AMI), locate Amazon Linux 2 AMI (HVM), SSD Volume Type, and then choose Select. 
4.	On the Step 2: Choose an Instance Type page, choose the free tier eligible t2.micro type as the hardware configuration for your instance, and then choose Next:     Configure Instance Details.
5.	On the Step 3: Configure Instance Details page, do the following:
•	In Number of instances, enter 1.
•	In Auto-assign Public IP, choose Enable.
•	In IAM role, choose the IAM role you created in the previous procedure (for example, EC2InstanceRole).
6.	Expand Advanced Details, and in the User data field, enter the following:
            #!/bin/bash
            yum -y update
            yum install -y ruby
            yum install -y aws-cli
            cd /home/ec2-user
            aws s3 cp s3://aws-codedeploy-us-east-2/latest/install . --region us-east-2
            chmod +x ./install
            ./install auto
    This code installs the CodeDeploy agent on your instance as it is created.
7.	Leave the rest of the items on the Step 3: Configure Instance Details page unchanged. Choose Next: Add Storage, leave the          Step 4: Add Storage page unchanged,and then choose Next: Add Tags.
8.	Choose Add Tag. In Key, enter Name, and in Value, enter MyCodePipelineDemo. Choose Next: Configure Security Group. Later,      you create a Code Deploy application that deploys the sample application to this instance. Code Deploy selects instances to      deploy based on the tags that are attached to instances.
9.	On the Step 6: Configure Security Group page, do the following:
        •	Next to Assign a security group, choose Create a new security group.
        •	In the row for SSH, under Source, choose My IP.
        •	Choose Add Rule, choose HTTP, and then under Source, choose My IP.
10.	Choose Review and Launch.

### Step 5: Create an Application in Code Deploy
   
####    Step 5A : To create a Code Deploy service role

1.	Open the IAM console at https://console.aws.amazon.com/iam/).
2.	From the console dashboard, choose Roles.
3.	Choose Create role.
4.	Under Select type of trusted entity, select AWS service. Under Choose a use case, select Code Deploy, and then choose Next: Permissions. The AWSCodeDeployRole      managed policy is already attached to the role.
5.	Choose Next: Tags, and Next: Review.
6.	Enter a name for the role (for example, CodeDeployRole), and then choose Create role.

####   Step 5B : To create an application in Code Deploy

1.	Open the CodeDeploy console at https://console.aws.amazon.com/codedeploy.
2.	If the Applications page does not appear, on the AWS CodeDeploy menu, choose Applications.
3.	Choose Create application.
4.	Leave Custom application selected. In Application name, enter MyDemoApplication.
5.	In Compute Platform, choose EC2/On-premises.
6.	Choose Create application

####   Step 5C : To create a deployment group in CodeDeploy

1.	On the page that displays your application, choose Create deployment group.
2.	In Deployment group name, enter MyDemoDeploymentGroup.
3.	In Service Role, choose the service role you created earlier (for example, CodeDeployRole).
4.	Under Deployment type, choose In-place.
5.	Under Environment configuration, choose Amazon EC2 Instances. In the Key field, enter the tag key you used to tag the instance (for example, MyCodePipelineDemo)
6.	Under Deployment configuration, choose CodeDeployDefault.OneAtaTime.
7.	Under Load Balancer, clear Enable load balancing. You do not need to set up a load balancer or choose a target group for this example.
8.	Expand the Advanced section. Under Alarms, if any alarms are listed, choose Ignore alarm configuration.
9.	Choose Create deployment group.

### Step 6: To create a Code Pipeline pipeline

1.	Sign in to the AWS Management Console and open the CodePipeline console at http://console.aws.amazon.com/codesuite/codepipeline/home.
     Open the CodePipeline console at https://console.aws.amazon.com/codepipeline/.
2.	Choose Create pipeline.
3.	In Step 1: Choose pipeline settings, in Pipeline name, enter MyFirstPipeline.
4.	In Service role, choose New service role to allow CodePipeline to create a service role in IAM.
5.	Leave the settings under Advanced settings at their defaults, and then choose Next.
6.	In Step 2: Add source stage, in Source provider, choose AWS Code Commit. In Repository name, choose the name of the Code Commit repository you created in step       1. In Branch name, choose master, and then choose Next step.
7.	In Step 3: Add build stage, choose Skip build stage, and then accept the warning message by choosing Skip again. Choose Next.
    Note
8.	In Step 4: Add deploy stage, in Deploy provider, choose AWS CodeDeploy. In Application name, choose MyDemoApplication. In Deployment group, choose                     MyDemoDeploymentGroup, and then choose Next step.
9.	In Step 5: Review, review the information, and then choose Create pipeline.
10.	The pipeline starts running after it is created. It downloads the code from your Code Commit repository and creates a Code Deploy deployment to your EC2            instance. You can view progress and success and failure messages as the Code Pipeline sample deploys the webpage to the Amazon EC2 instance in the Code Deploy      deployment.
 
Thanks!
